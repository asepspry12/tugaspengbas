<?php
//Mulai Sesion
session_start();
if (isset($_SESSION["ses_username"]) == "") {
	header("location: login.php");
} else {
	$data_id = $_SESSION["ses_id"];
	// $data_nama = $_SESSION["ses_nama"];
	$data_user = $_SESSION["ses_username"];
	$data_level = $_SESSION["ses_level"];
}

//KONEKSI DB
include "koneksi.php";
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Barokah Motor</title>
	<link rel="icon" href="dist/img/loghit.png">
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css"> -->

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->

	<!-- Ionicons -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
	<!-- Select2 -->
	<link rel="stylesheet" href="plugins/select2/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
	

</head>

<body class="hold-transition skin-green sidebar-mini" style="background-position: fixed;">
	<!-- Site wrapper -->
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="index.php" class="logo">
				<span class="logo-lg">
					<img src="dist/img/log.png" width="37px">
					<b>Barokah Motor</b>
				</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<li class="dropdown messages-menu">
							<a href="logout.php" onclick="return confirm('Anda yakin keluar dari aplikasi ?')">
								<i class="fa fa-sign-out"></i>
								<span>Logout</span>
								<span class="pull-right-container"></span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>

		<!-- =============================================== -->

		<!-- Left side column. contains the sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->

				<ul class="sidebar-menu">

					<!-- Level awal -->
					<!-- pemilik awal -->
					<?php
					if ($data_level == "pemilik") {
					?>
						<!-- Sidebar user panel -->
						<div class="user-panel">
							<div class="pull-left image">
								<img src="dist/img/avatar.png" class="img-circle" alt="User Image">
							</div>
							<div class="pull-left info">
								<p>
									<?php echo ucwords($data_user); ?>
								</p>
								<span class="label label-warning">
									<?php echo ucwords($data_level); ?>
								</span>
							</div>
						</div>
						</br>
						<!-- Sidebar user panel -->

						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview">
							<a href="?page=pemilik">
								<i class="fa fa-dashboard"></i>
								<span>Dashboard</span>
								<span class="pull-right-container"></span>
							</a>
						</li>

						</li>

						<li class="header">Motor</li>
						<li class="treeview">
							<a href="?page=pemilik/data_motor">
								<i class="fa fa-motorcycle"></i>
								<span>Indentitas Motor</span>
								<span class="pull-right-container">
								</span>
							</a>
						</li>

						</li>

						<li class="header">Pengguna</li>
						<li class="treeview">
							<a href="?page=pemilik/data_pengguna">
								<i class="fa fa-user"></i>
								<span>Menu Pengguna</span>
								<span class="pull-right-container">
								</span>
							</a>
						</li>

						</li>
						<!-- pemilik akhir -->

						<!-- teller awal -->
					<?php
					} elseif ($data_level == "teller") {
					?>

						<!-- Sidebar user panel -->
						<div class="user-panel">
							<div class="pull-left image">
								<img src="dist/img/teller.png" class="img-circle" alt="User Image">
							</div>
							<div class="pull-left info">
								<p>
									<?php echo ucwords($data_user); ?>
								</p>
								<span class="label label-warning">
									<?php echo ucwords($data_level); ?>
								</span>
							</div>
						</div>
						</br>
						<!-- Sidebar user panel -->
						</li>
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview">
							<a href="?page=teller">
								<i class="fa fa-dashboard"></i>
								<span>Dashboard</span>
								<span class="pull-right-container"></span>
							</a>
						</li>

						</li>
						<li class="header">Motor</li>
						<li class="treeview">
							<a href="?page=teller/data_motor">
								<i class="fa fa-motorcycle"></i>
								<span>Indentitas Motor</span>
								<span class="pull-right-container">
								</span>
							</a>
						</li>
						</li>

						<li class="header">Pengguna</li>
						<li class="treeview">
							<a href="?page=teller/data_pengguna">
								<i class="fa fa-user"></i>
								<span>Menu Pengguna</span>
								<span class="pull-right-container">
								</span>
							</a>
						</li>

						<li class="treeview">
							<a href="#">
								<i class="fa fa-money"></i>
								<span>Menu Transaksi</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu"> 
								<li>
									<a href="?page=teller/transaksi1">
									<i class="fa fa-motorcycle"></i>Menu Daftar Barang</a>
								</li>
								<li>
									<a href="?page=teller/transaksi">
									<i class="fa fa-money"></i>Menu Transaksi Penjualan</a>
								</li>
							</ul>
						</li>

						</li>
						<!-- teller akhir -->

						<!-- teknisi awal -->

					<?php
					} elseif ($data_level == "teknisi") {
					?>

						<!-- Sidebar user panel -->
						<div class="user-panel">
							<div class="pull-left image">
								<img src="dist/img/teknisi.png" class="img-circle" alt="User Image">
							</div>
							<div class="pull-left info">
								<p>
									<?php echo ucwords($data_user); ?>
								</p>
								<span class="label label-warning">
									<?php echo ucwords($data_level); ?>
								</span>
							</div>
						</div>
						</br>
						<!-- Sidebar user panel -->

						</li>
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview">
							<a href="?page=teknisi">
								<i class="fa fa-dashboard"></i>
								<span>Dashboard</span>
								<span class="pull-right-container"></span>
							</a>
						</li>

						<li class="treeview">
							<a href="#">
								<i class="fa fa-money"></i>
								<span>Menu Transaksi</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li>
									<a href="?page=teknisi/transaksi">
									<i class="fa fa-motorcycle"></i>Menu Daftar Barang</a>
								</li>
								<li>
									<a href="?page=teknisi/transaksi1">
									<i class="fa fa-money"></i>Menu Transaksi Penjualan</a>
								</li>
							</ul>
						</li>

						<!-- <li class="header">Transaksi</li>
						<li class="treeview">
							<a href="?page=teknisi/data_transaksi">
								<i class="fa fa-money"></i>
								<span>Transaksi</span>
								<span class="pull-right-container">
								</span>
							</a>
						</li> -->

						</li>
						<!-- teknisi akhir -->

						<!-- customer awal -->


					<?php
					} elseif ($data_level == "customer") {
					?>

						<!-- Sidebar user panel -->
						<div class="user-panel">
							<div class="pull-left image">
								<img src="dist/img/customer.png" class="img-circle" alt="User Image">
							</div>
							<div class="pull-left info">
								<p>
									<?php echo ucwords($data_user); ?>
								</p>
								<span class="label label-warning">
									<?php echo ucwords($data_level); ?>
								</span>
							</div>
						</div>
						</br>
						<!-- Sidebar user panel -->

						</li>
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview">
							<a href="?page=customer">
								<i class="fa fa-dashboard"></i>
								<span>Dashboard</span>
								<span class="pull-right-container"></span>
							</a>
						</li>

						<li class="treeview">
							<a href="#">
								<i class="fa fa-money"></i>
								<span>Menu Transaksi</span>
								<span class="pull-right-container">
									<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">

								<li>
									<a href="?page=customer/transaksi">
									<i class="fa fa-motorcycle"></i>Menu Daftar Barang</a>
								</li>
								
							</ul>
						</li>
						</li>
					<?php
					}
					?>
					<!-- customer akhir -->


					<!-- Level akhir -->

			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- =============================================== -->

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<!-- Main content -->
			<section class="content">
				<?php
				if (isset($_GET['page'])) {
					$hal = $_GET['page'];

					switch ($hal) {

							//Halaman Home
						case 'pemilik':
							include "home/pemilik.php";
							break;
						case 'teller':
							include "home/teller.php";
							break;
						case 'teknisi':
							include "home/teknisi.php";
							break;
						case 'customer':
							include "home/customer.php";
							break;


							//Klik Halaman pemilik awal

							//Klik Halaman pemilik bagian indentitas awal
						case 'pemilik/data_motor':
							include "pemilik/motor/data_motor.php";
							break;
						case 'pemilik/add_motor':
							include "pemilik/motor/add_motor.php";
							break;
						case 'pemilik/edit_motor':
							include "pemilik/motor/edit_motor.php";
							break;
						case 'pemilik/del_motor':
							include "pemilik/motor/del_motor.php";
							break;
							//Klik Halaman pemilik bagian indentitas akhir

							//Klik Halaman pemilik bagian pengguna awal
						case 'pemilik/data_pengguna':
							include "pemilik/pengguna/data_pengguna.php";
							break;
						case 'pemilik/add_pengguna':
							include "pemilik/pengguna/add_pengguna.php";
							break;
						case 'pemilik/edit_pengguna':
							include "pemilik/pengguna/edit_pengguna.php";
							break;
						case 'pemilik/del_pengguna':
							include "pemilik/pengguna/del_pengguna.php";
							break;
							//Klik Halaman pemilik bagian pengguna awal

							//Klik Halaman pemilik akhir

							//Klik Halaman teller awal

							//teller motor
						case 'teller/data_motor':
							include "Teller/motor/data_motor.php";
							break;
						// case 'teller/add_motor':
						// 	include "Teller/motor/add_motor.php";
						// 	break;
						// case 'teller/edit_motor':
						// 	include "Teller/motor/edit_motor.php";
						// 	break;
						// case 'teller/del_motor':
						// 	include "Teller/motor/del_motor.php";
						// 	break;

							//teller pengguna
						case 'teller/data_pengguna':
							include "Teller/pengguna/data_pengguna.php";
							break;
						case 'teller/add_pengguna':
							include "Teller/pengguna/add_pengguna.php";
							break;
						case 'teller/edit_pengguna':
							include "Teller/pengguna/edit_pengguna.php";
							break;
						case 'teller/del_pengguna':
							include "Teller/pengguna/del_pengguna.php";
							break;

							//customer transaksi1 awal
							case 'teller/transaksi1':
								include "Teller/penjualan/transaksi.php";
								break;
							case 'teller/detail_transaksi1':
								include "Teller/penjualan/detail_transaksi.php";
								break;
								//customer transaksi1 akhir

							//customer transaksi awal
							case 'teller/transaksi':
								include "Teller/transaksi/transaksi.php";
								break;
							case 'teller/edit_transaksi':
								include "Teller/transaksi/edit_transaksi.php";
								break;
							case 'teller/del_transaksi':
								include "Teller/transaksi/del_transaksi.php";
								break;		
							//customer transaksi akhir

						//Klik Halaman teller akhir							

							// halaman kosong

							//teknisi
						case 'teknisi/transaksi':
								include "teknisi/transaksi.php";
								break;
						case 'teknisi/detail_transaksi':
								include "teknisi/detail_transaksi.php";
								break;
						case 'teknisi/del_transaksi':
								include "teknisi/del_transaksi.php";
								break;

						case 'teknisi/transaksi1':
							include "teknisi/transaksi/transaksi.php";
							break;
						case 'teknisi/del_transaksi1':
							include "teknisi/transaksi/del_transaksi.php";
							break;
						
						//Halaman Customer Awal

							//customer transaksi awal
							case 'customer/transaksi':
								include "customer/transaksi.php";
								break;
							case 'customer/detail_transaksi':
								include "customer/detail_transaksi.php";
								break;
							// case 'customer/detail':
							// 	include "customer/detail.php";
							// 	break;	
								//customer transaksi akhir
	
						//Halaman Customer Akhir

						default:
							echo "<center><br><br><br><br><br><br><br><br><br>
				  <h1> Halaman tidak ditemukan !</h1></center>";
							break;
					}
				} else {
					// Auto Halaman Home Pengguna
					if ($data_level == "pemilik") {
						include "home/pemilik.php";
					} elseif ($data_level == "teller") {
						include "home/teller.php";
					} elseif ($data_level == "teknisi") {
						include "home/teknisi.php";
					} elseif ($data_level == "customer") {
						include "home/customer.php";
					}
				}
				?>



			</section>
			<!-- /.content -->
		</div>

		<!-- /.content-wrapper -->
		<div class="control-sidebar-bg"></div>

		<!-- ./wrapper -->

		<!-- jQuery 2.2.3 -->
		<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="bootstrap/js/bootstrap.min.js"></script>

		<script src="plugins/select2/select2.full.min.js"></script>
		<!-- DataTables -->
		<script src="plugins/datatables/jquery.dataTables.min.js"></script>
		<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

		<!-- AdminLTE App -->
		<script src="dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="dist/js/demo.js"></script>
		<!-- page script -->


		<script>
			$(function() {
				$("#example1").DataTable();
				$('#example2').DataTable({
					"paging": true,
					"lengthChange": false,
					"searching": false,
					"ordering": true,
					"info": true,
					"autoWidth": false
				});
			});
		</script>

		<script>
			$(function() {
				//Initialize Select2 Elements
				$(".select2").select2();
			});
		</script>
</body>

</html>
