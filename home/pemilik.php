<?php
$sql = $koneksi->query("SELECT count(ID) as motor from identitas_motor");
while ($data = $sql->fetch_assoc()) {

	$motor = $data['motor'];
}
?>

<?php
$sql = $koneksi->query("SELECT count(IDUser) as anggota from user");
while ($data = $sql->fetch_assoc()) {

	$anggota = $data['anggota'];
}
?>

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dashboard
		<small>Administrator</small>
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">

		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-blue">
				<div class="inner">
					<h4>
						<?= $motor; ?>
					</h4>

					<p>Motor</p>
				</div>
				<div class="icon">
					<i class="fa fa-motorcycle"></i>
				</div>
				<a href="?page=pemilik/data_motor" class="small-box-footer">More info
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>

		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h4>
						<?= $anggota; ?>
					</h4>

					<p>Pengguna</p>
				</div>
				<div class="icon">
					<i class="ion ion-person-add"></i>
				</div>
				<a href="?page=pemilik/data_pengguna" class="small-box-footer">More info
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>