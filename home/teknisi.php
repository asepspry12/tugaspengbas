<?php
$sql = $koneksi->query("SELECT count(IdTrsk) as transaksi from transaksi");
while ($data = $sql->fetch_assoc()) {

	$transaksi = $data['transaksi'];
}
?>

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Dashboard
		<small>Teknisi</small>
	</h1>
</section>

<!-- Main content -->
<section class="content">
	<!-- Small boxes (Stat box) -->
	<div class="row">

		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
					<h4>
						<?= $transaksi; ?>
					</h4>

					<p>Transaksi</p>
				</div>
				<div class="icon">
					<i class="fa fa-money"></i>
				</div>
				<a href="?page=teknisi/transaksi1" class="small-box-footer">More info
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</div>
		</div>