<?php
if ($_SESSION["ses_level"] !== "pemilik") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

if (isset($_GET['IDUser'])) {
	$sql_cek = "SELECT * FROM user INNER JOIN customer ON user.IDUser=customer.IdCust WHERE IDUser = '".$_GET['IDUser']."' ";
	$query_cek = mysqli_query($koneksi, $sql_cek);
	$data_cek = mysqli_fetch_assoc($query_cek);
	// $sql_cek = "SELECT * FROM user ORDER BY IDUser DESC ";
	// $query_cek = mysqli_query($koneksi, $sql_cek);
	// $data_cek = mysqli_fetch_array($query_cek);
} 
	// elseif (isset($_GET['IDUser']) && isset($_POST['Hak_Akses'] ) == 'customer' ) {
// 	$sql_cek = "SELECT * FROM user,customer WHERE user.IDUser=customer.IdCust ";
// 	$query_cek = mysqli_query($koneksi, $sql_cek);
// 	$data_cek = mysqli_fetch_array($query_cek);
// }


// if (isset($_GET['kode'])) {
// 	$sql_cek = "SELECT * FROM user WHERE IDUser='" . $_GET['kode'] . "'";
// 	$query_cek = mysqli_query($koneksi, $sql_cek);
// 	$data_cek = mysqli_fetch_array($query_cek, MYSQLI_BOTH);
// }

?>

<section class="content-header">
	<h1>
		Pengguna Sistem
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>

				<b>Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Ubah Pengguna</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove">
							<i class="fa fa-remove"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="" method="post" enctype="multipart/form-data">
					<div class="box-body">

						<!-- <div class="form-group">
							<input type='hidden' class="form-control" name="IDUser" value="<?php echo $data_cek['IDUser']; ?>"
							 readonly/>
						</div> -->

						<div class="form-group">

							<label>ID Pengguna</label>
							<input class="form-control" name="IDUser" value="<?php echo $data_cek['IDUser']; ?>" readonly />
						</div>

						<div class="form-group">
							<label>Nama Pengguna</label>
							<input class="form-control" name="Nama" value="<?php echo $data_cek['Nama']; ?>" />
						</div>


						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="password" class="form-control" name="Password" id="Password" value="<?php echo $data_cek['Password']; ?>" />
							<input id="mybutton" onclick="change()" type="checkbox" class="form-checkbox"> Lihat Password
						</div>

						<div class="form-group">
							<label>Level</label>
							<select name="Hak_Akses" id="Hak_Akses" class="form-control" required>
								<option value="">-- Pilih Hak_Akses --</option>
								<?php
								//menhecek data yg dipilih sebelumnya

								if ($data_cek['Hak_Akses'] == "pemilik") echo "<option value='pemilik' selected>Pemilik</option>";
								else echo "<option value='pemilik'>Pemilik</option>";

								if ($data_cek['Hak_Akses'] == "teller") echo "<option value='teller' selected>Teller</option>";
								else echo "<option value='teller'>Teller</option>";

								if ($data_cek['Hak_Akses'] == "teknisi") echo "<option value='teknisi' selected>Teknisi</option>";
								else echo "<option value='teknisi'>Teknisi</option>";

								if ($data_cek['Hak_Akses'] == "customer") echo "<option value='customer' selected>Customer</option>";
								else echo "<option value='customer'>Customer</option>";

								?>
							</select>
						</div>

						<div class="form-group">
							<label>Tanggal Dibuat</label>
							<input class="form-control" name="Create_Date" value="<?php echo $data_cek['Create_Date']; ?>" readonly />
						</div>

						<div class="form-group">
							<label>Jabatan</label>
							<input class="form-control" name="Jabatan" value="<?php echo $data_cek['Jabatan']; ?>" />
						</div>

						<div class="form-group">
							<label>Alamat</label>
							<input class="form-control" name="AlamatCust" value="<?php echo $data_cek['AlamatCust']; ?>" />
						</div>

						<div class="form-group">
							<label>No Telepon</label>
							<input class="form-control" name="TelpCust" value="<?php echo $data_cek['TelpCust']; ?>" />
						</div>

						<div class="form-group">
							<label>NIK</label>
							<input class="form-control" name="NIKCust" value="<?php echo $data_cek['NIKCust']; ?>" />
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<input type="submit" name="Ubah" value="Ubah" class="btn btn-success">
						<a href="?page=pemilik/data_pengguna" title="Kembali" class="btn btn-warning">Batal</a>
					</div>
				</form>
			</div>
			<!-- /.box -->
</section>

<?php


if (isset($_POST['Ubah'])) {
	//mulai proses ubah
	$sql_ubah = "UPDATE user SET
        Nama 				='" . $_POST['Nama'] . "',
        Password		='" . $_POST['Password'] . "',
        Hak_Akses		='" . $_POST['Hak_Akses'] . "',
        Jabatan			='" . $_POST['Jabatan'] . "'
        WHERE IDUser='" . $_POST['IDUser'] . "' ";
	$query_ubah = mysqli_query($koneksi, $sql_ubah);

	$sql_ubah2 = "UPDATE customer SET 
				NamaCust 			='" . $_POST['Nama'] . "',
				AlamatCust		='" . $_POST['AlamatCust'] . "',
        TelpCust			='" . $_POST['TelpCust'] . "',
        NIKCust				='" . $_POST['NIKCust'] . "'
        WHERE IdCust	='" . $_POST['IDUser'] . "' ";
	$query_ubah2 = mysqli_query($koneksi, $sql_ubah2);
	

	if ($query_ubah && $query_ubah2) {
		echo "<script>

      Swal.fire({title: 'Ubah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
      }).then((result) => {
          if (result.value) {
              window.location = 'index.php?page=pemilik/data_pengguna';
          }
      })</script>";
	} else {
		echo "<script>
      Swal.fire({title: 'Ubah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
      }).then((result) => {
          if (result.value) {
              window.location = 'index.php?page=pemilik/data_pengguna';
          }
      })</script>";
	}

	//selesai proses ubah
}

?>

<script type="text/javascript">
	function change() {
		var x = document.getElementById('Password').type;

		if (x == 'password') {
			document.getElementById('Password').type = 'text';
			document.getElementById('mybutton').innerHTML;
		} else {
			document.getElementById('Password').type = 'password';
			document.getElementById('mybutton').innerHTML;
		}
	}
</script>
