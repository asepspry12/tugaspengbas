<?php
if ($_SESSION["ses_level"] !== "pemilik") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

?>

<section class="content-header">
	<h1>
		Pengguna Sistem
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>

				<b>Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<a href="?page=pemilik/add_pengguna" class="btn btn-primary">
				<i class="glyphicon glyphicon-plus"></i> Tambah Data</a>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="table-responsive">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>

							<th>Id Pengguna</th>
							<th>Nama</th>
							<th>Password</th>
							<th>Hak Akses</th>
							<th>Tanggal Dibuat</th>
							<th>Jabatan</th>
							<th>Alamat</th>
							<th>No Telepon</th>
							<th>NIK</th>
							<th>Aksi</th>

						</tr>
					</thead>
					<tbody>


						<?php
						$no = 1;
						$sql = $koneksi->query("SELECT * FROM user,customer WHERE user.IDUser=customer.IdCust");
						while ($data = $sql->fetch_assoc()) {
						?>

							<tr>
								<td>
									<?php echo $no++; ?>
								</td>
								<td>
									<?php echo $data['IDUser']; ?>
								</td>
								<td>
									<?php echo $data['Nama']; ?>
								</td>
								<td>
									<?php echo $data['Password']; ?>
								</td>
								<td>
									<?php echo $data['Hak_Akses']; ?>
								</td>
								<td>
									<?php echo $data['Create_Date']; ?>
								</td>
								<td>
									<?php echo $data['Jabatan']; ?>
								</td>
								<td>
									<?php echo $data['AlamatCust']; ?>
								</td>
								<td>
									<?php echo $data['TelpCust']; ?>
								</td>
								<td>
									<?php echo $data['NIKCust']; ?>
								</td>


								<td>
									<a href="?page=pemilik/edit_pengguna&IDUser=<?php echo $data['IDUser']; ?>" title="Ubah" class="btn btn-success">
										<i class="glyphicon glyphicon-edit"></i>
									</a>
									<a href="?page=pemilik/del_pengguna&IDUser=<?php echo $data['IDUser']; ?>" onclick="return confirm('Apakah anda yakin hapus data ini ?')" title="Hapus" class="btn btn-danger">
										<i class="glyphicon glyphicon-trash"></i>
									</a>
								</td>
							</tr>

						<?php
						}
						?>
					</tbody>

				</table>
			</div>
		</div>
	</div>
</section>