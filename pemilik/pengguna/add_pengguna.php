<?php
if ($_SESSION["ses_level"] !== "pemilik") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

$carikode = mysqli_query($koneksi, "SELECT MAX(IDUser) AS formatid FROM user order by IDUser desc");
$datakode = mysqli_fetch_array($carikode);
$kode = $datakode['formatid'];
$urut = (int) substr($kode, 1, 3);
$urut++;

$char = '9';
$id_user = $char . sprintf("%03s",$urut);

$tambah = mysqli_query($koneksi, "SELECT * FROM user");

date_default_timezone_set("Asia/Jakarta");
$Create_Date = date("Y-m-d H:i:s");

?>
<section class="content-header">
	<h1>
		Pengguna Sistem
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Tambah Pengguna</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove">
							<i class="fa fa-remove"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="" method="post" enctype="multipart/form-data">
					<div class="box-body">

						<div class="form-group">
							<label>ID User</label>
							<input type="number" name="IDUser" id="IDUser" class="form-control" value="<?php echo $id_user; ?>" readonly />
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Nama Pengguna</label>
							<input type="text" name="Nama" id="Nama" class="form-control" placeholder="Nama pengguna" required>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							<input type="Password" name="Password" id="Password" class="form-control" placeholder="Password" required>
						</div>

						<div class="form-group">
							<label>Hak Akses</label>
							<select name="Hak_Akses" id="Hak_Akses" class="form-control" required>
								<option>-- Pilih Hak Akses --</option>
								<option value="pemilik">Pemilik</option>
								<option value="teller">Teller</option>
								<option value="teknisi">Teknisi</option>
								<option value="customer">Customer</option>
							</select>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Jabatan</label>
							<input type="text" name="Jabatan" id="Jabatan" class="form-control" placeholder="Jabatan" required>
						</div>

					
					<!-- if($_POST['Hak_Akses'] == 'customer'){ -->

						
						<div class="form-group">
							<label for="exampleInputEmail1">Alamat</label>
							<input type="text" name="AlamatCust" id="AlamatCust" class="form-control" placeholder="Alamat">
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">No Telpon</label>
							<input type="number" name="TelpCust" id="TelpCust" class="form-control" placeholder="08xxxxxxxxxxx" >
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">NIK</label>
							<input type="number" name="NIKCust" id="NIKCust" class="form-control" placeholder="16 Digit" >
						</div>

				
					<!-- <div class="form-group">
							<label for="exampleInputEmail1">Alamat</label>
							<input type="text" name="Alamat" id="Alamat" class="form-control" placeholder="Alamat" readonly>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">No Telpon</label>
							<input type="number" name="Notelp" id="Notelp" class="form-control" placeholder="08xxxxxxxxxxx" readonly>
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">NIK</label>
							<input type="number" name="NIK" id="NIK" class="form-control" placeholder="16 Digit" readonly>
						</div> -->

						

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<input type="submit" name="Simpan" value="Simpan" class="btn btn-info">
						<a href="?page=pemilik/data_pengguna" title="Kembali" class="btn btn-warning">Batal</a>
					</div>
				</form>
			</div>
			<!-- /.box -->
</section>

<?php


if (isset($_POST['Simpan'])) {
	//mulai proses simpan data
	$sql_simpan = "INSERT INTO user (IDUser,Nama,Password,Hak_Akses,Jabatan) VALUES (
        '" . $id_user . "',
        '" . $_POST['Nama'] . "',
        '" . $_POST['Password'] . "',
        '" . $_POST['Hak_Akses'] . "',
        '" . $_POST['Jabatan'] . "'
      	)";
	$query_simpan = mysqli_query($koneksi, $sql_simpan);

	$sql_simpan2 = "INSERT INTO customer (IdCust,NamaCust,AlamatCust,TelpCust,NIKCust) VALUES (
				'" . $id_user . "',
				'" . $_POST['Nama'] . "',
				'" . $_POST['AlamatCust'] . "',
				'" . $_POST['TelpCust'] . "',
				'" . $_POST['NIKCust'] . "'
				)";
	$query_simpan2 = mysqli_query($koneksi, $sql_simpan2);		


	
	if ($query_simpan && $query_simpan2) {
		echo "<script>
      Swal.fire({title: 'Tambah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
      }).then((result) => {if (result.value){
        window.location = 'index.php?page=pemilik/data_pengguna';
        }
      })</script>";
		} 
		
		// elseif ($query_simpan && $query_simpan2) {
		// 	echo "<script>
  //     Swal.fire({title: 'Tambah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
  //     }).then((result) => {if (result.value){
  //       window.location = 'index.php?page=pemilik/data_pengguna';
  //       }
  //     })</script>";
		// }
	
		else {
		echo "<script>
      Swal.fire({title: 'Tambah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
      }).then((result) => {if (result.value){
        window.location = 'index.php?page=pemilik/add_pengguna';
        }
      })</script>";
		}
	//selesai proses simpan data
}
