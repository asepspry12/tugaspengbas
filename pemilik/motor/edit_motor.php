<?php
if ($_SESSION["ses_level"] !== "pemilik") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

// include 'inc/koneksi.php';
if (isset($_GET['ID'])) {
	$ID = $_GET['ID'];
	$sql_cek = "SELECT * FROM identitas_motor WHERE ID ='" . $ID . "' ORDER BY ID DESC ";
	$query_cek = mysqli_query($koneksi, $sql_cek);
	$data_cek = mysqli_fetch_array($query_cek, MYSQLI_BOTH);
}

$gid = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM identitas_motor ORDER BY ID DESC "));
?>

<section class="content-header">
	<h1>
		Master Data
		<small>Data Motor</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Ubah Motor</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove">
							<i class="fa fa-remove"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="" method="post" enctype="multipart/form-data">
					<div class="box-body">

						<div class="form-group">
							<label>Id Motor</label>
							<input type='text' class="form-control" name="ID" value="<?php echo $data_cek['ID']; ?>" readonly />
						</div>

						<div class="form-group">
							<label>No Registrasi</label>
							<input type='number' class="form-control" name="NoRegistrasi" value="<?php echo $data_cek['NoRegistrasi']; ?>" required />

						</div>

						<div class="form-group">
							<label>Nama Pemilik</label>
							<input type='text' class="form-control" name="NamaPemilik" value="<?php echo $data_cek['NamaPemilik']; ?>" required />

						</div>

						<div class="form-group">
							<label>Alamat</label>

							<input class="form-control" name="Alamat" value="<?php echo $data_cek['Alamat']; ?>" required />

						</div>

						<div class="form-group">
							<label>No Rangka</label>
							<input class="form-control" name="NoRangka" value="<?php echo $data_cek['NoRangka']; ?>" required />
						</div>

						<div class="form-group">
							<label>No Mesin</label>
							<input class="form-control" name="NoMesin" value="<?php echo $data_cek['NoMesin']; ?>" required />
						</div>

						<div class="form-group">

							<label>Plat Nomor</label>
							<input class="form-control" name="PlatNO" value="<?php echo $data_cek['PlatNO']; ?>" required />
						</div>

						<div class="form-group">
							<label>Merk</label>
							<input class="form-control" name="Merk" value="<?php echo $data_cek['Merk']; ?>" required />
						</div>

						<div class="form-group">
							<label>Tipe</label>
							<input class="form-control" name="Tipe" value="<?php echo $data_cek['Tipe']; ?>" required />

						</div>

						<div class="form-group">
							<label>Model</label>
							<input class="form-control" name="Model" value="<?php echo $data_cek['Model']; ?>" required />
						</div>

						<div class="form-group">
							<label>Tahun Pembuatan</label>
							<input class="form-control" name="TahunPembuatan" value="<?php echo $data_cek['TahunPembuatan']; ?>" required />
						</div>

						<div class="form-group">
							<label>Isi Silinder</label>
							<input class="form-control" name="IsiSilinder" value="<?php echo $data_cek['IsiSilinder']; ?>" required />
						</div>

						<div class="form-group">
							<label>Bahan Bakar</label>
							<input class="form-control" name="BahanBakar" value="<?php echo $data_cek['BahanBakar']; ?>" required />
						</div>

						<div class="form-group">
							<label>Warna TNKB</label>
							<input class="form-control" name="WarnaTNKB" value="<?php echo $data_cek['WarnaTNKB']; ?>" required />
						</div>

						<div class="form-group">
							<label>Tahun Registrasi</label>
							<input class="form-control" name="TahunRegistrasi" value="<?php echo $data_cek['TahunRegistrasi']; ?>" required />
						</div>

						<div class="form-group">
							<label>No BPKB</label>
							<input class="form-control" name="NoBPKB" value="<?php echo $data_cek['NoBPKB']; ?>" required />
						</div>

						<div class="form-group">
							<label>Kode Lokasi</label>
							<input class="form-control" name="KodeLokasi" value="<?php echo $data_cek['KodeLokasi']; ?>" required />
						</div>

						<div class="form-group">
							<label>Masa Berlaku STNK</label>
							<input class="form-control" name="MasaBerlakuSTNK" value="<?php echo $data_cek['MasaBerlakuSTNK']; ?>" required />
						</div>

						<div class="form-group">
							<label>Gambar Motor</label>
							<img src="foto_motor/<?= $data_cek['ID']; ?>.png" style="width: 100px; height: 100px;">
							<input type="file" name="GambarMotor[]" id="GambarMotor" class="form-control" required>
						</div>

						<div class="form-group">
							<label>Tanggal Beli</label>
							<input type="date" class="form-control" name="TglBeli" value="<?php echo $data_cek['TglBeli']; ?>" required />
						</div>

						<div class="form-group">
							<label>Harga Beli</label>
							<input class="form-control" name="HargaBeli" value="<?php echo $data_cek['HargaBeli']; ?>" required />
						</div>

						<div class="form-group">
							<label>Harga Jual</label>
							<input class="form-control" name="HargaJual" value="<?php echo $data_cek['HargaJual']; ?>" required />
						</div>



					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<input type="submit" name="Ubah" value="Ubah" class="btn btn-success">
						<a href="?page=pemilik/data_motor" class="btn btn-warning">Batal</a>
					</div>
				</form>
			</div>
			<!-- /.box -->
</section>

<?php


if (isset($_POST['Ubah'])) {
	//mulai proses ubah
  $i = $_GET['ID'];
  $id = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM identitas_motor WHERE ID = '$i' "));

  extract($_POST);
  $error = array();
  $extension = array("jpeg", "jpg", "png");
  $gid = $id['ID'];
  foreach ($_FILES["GambarMotor"]["tmp_name"] as $key => $tmp_name) {
    
    $file_name2 = $_FILES["GambarMotor"]["name"][$key];
    $ext2 = pathinfo($file_name2,PATHINFO_EXTENSION);
    // $file_name = $id.'_'.$no.'.'.$ext2;
    $file_name = $gid.'.png';
    $file_tmp = $_FILES["GambarMotor"]["tmp_name"][$key];
    $ext = pathinfo($file_name,PATHINFO_EXTENSION);
    if(in_array($ext, $extension)) {
      if(!file_exists("foto_motor/".$file_name)) {
        move_uploaded_file($file_tmp = $_FILES["GambarMotor"]["tmp_name"][$key],"foto_motor/".$file_name);
      } 
      else {
        $filename = basename($file_name, $ext);
        $newFileName = $filename.$ext;
        move_uploaded_file($file_tmp=$_FILES["GambarMotor"]["tmp_name"][$key],"foto_motor/".$newFileName);
      }
    } 
    else {
      array_push($error, "$file_name, ");
    }
  }  

	$sql_ubah = "UPDATE identitas_motor SET
        NoRegistrasi ='" . $_POST['NoRegistrasi'] . "',
        NamaPemilik ='" . $_POST['NamaPemilik'] . "',
        Alamat ='" . $_POST['Alamat'] . "', 
        NoRangka ='" . $_POST['NoRangka'] . "', 
        NoMesin ='" . $_POST['NoMesin'] . "', 
        PlatNO ='" . $_POST['PlatNO'] . "', 
        Merk ='" . $_POST['Merk'] . "', 
        Tipe ='" . $_POST['Tipe'] . "', 
        Model ='" . $_POST['Model'] . "', 
        TahunPembuatan ='" . $_POST['TahunPembuatan'] . "', 
        IsiSilinder ='" . $_POST['IsiSilinder'] . "', 
        BahanBakar ='" . $_POST['BahanBakar'] . "', 
        WarnaTNKB ='" . $_POST['WarnaTNKB'] . "', 
        TahunRegistrasi ='" . $_POST['TahunRegistrasi'] . "', 
        NoBPKB ='" . $_POST['NoBPKB'] . "', 
        KodeLokasi ='" . $_POST['KodeLokasi'] . "', 
        MasaBerlakuSTNK ='" . $_POST['MasaBerlakuSTNK'] . "',
        TglBeli = '".$_POST['TglBeli']."',
        HargaBeli = '".$_POST['HargaBeli']."',
        HargaJual = '".$_POST['HargaJual']."' WHERE ID ='" . $_POST['ID'] . "' ";
	$query_ubah = mysqli_query($koneksi, $sql_ubah);

	if ($query_ubah) {
		echo "<script>
	    Swal.fire({title: 'Ubah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
	    }).then((result) => {
	        if (result.value) {
	            window.location = 'index.php?page=pemilik/data_motor';
	        }
	    })</script>";
	} 
	else {
		echo "<script>
	    Swal.fire({title: 'Ubah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
	    }).then((result) => {
	        if (result.value) {
	            window.location = 'index.php?page=pemilik/data_motor';
	        }
	    })</script>";
	}
   
  }
