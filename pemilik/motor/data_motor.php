<?php
if ($_SESSION["ses_level"] !== "pemilik") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

?>

<section class="content-header">
	<h1>
		Master Data
		<small>Data Motor</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Si Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<a href="?page=pemilik/add_motor" title="Tambah Data" class="btn btn-primary">
				<i class="glyphicon glyphicon-plus"></i> Tambah Data</a>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="table-responsive">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>ID</th>
							<th>No Registrasi</th>
							<th>Nama Pemilik</th>
							<th>Alamat</th>
							<th>No Rangka</th>
							<th>No Mesin</th>
							<th>Plat No</th>
							<th>Merk</th>
							<th>Type</th>
							<th>Model</th>
							<th>Tahun Pembuatan</th>
							<th>Isi Silinder</th>
							<th>Bahan Bakar</th>
							<th>Warna Motor</th>
							<th>Tahun Registrasi</th>
							<th>No BPKB</th>
							<th>Kode Lokasi</th>
							<th>Masa Berlaku STNK</th>
							<th>Gambar</th>
							<th>Tanggal Beli</th>
							<th>Harga Beli</th>
							<th>Tanggal Jual</th>
							<th>Harga Jual</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>

						<?php
						$no = 1;
						$gid = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM identitas_motor ORDER BY ID DESC"));
						$sql = $koneksi->query("SELECT * from identitas_motor");
						while ($data = $sql->fetch_assoc()) {
						?>

							<tr>
								<td>
									<?php echo $no++; ?>
								</td>
								<td>
									<?php echo $data['ID']; ?>
								</td>
								<td>
									<?php echo $data['NoRegistrasi']; ?>
								</td>
								<td>
									<?php echo $data['NamaPemilik']; ?>
								</td>
								<td>
									<?php echo $data['Alamat']; ?>
								</td>
								<td>
									<?php echo $data['NoRangka']; ?>
								</td>
								<td>
									<?php echo $data['NoMesin']; ?>
								</td>
								<td>
									<?php echo $data['PlatNO']; ?>
								</td>
								<td>
									<?php echo $data['Merk']; ?>
								</td>
								<td>
									<?php echo $data['Tipe']; ?>
								</td>
								<td>
									<?php echo $data['Model']; ?>
								</td>
								<td>
									<?php echo $data['TahunPembuatan']; ?>
								</td>
								<td>
									<?php echo $data['IsiSilinder']; ?>
								</td>
								<td>
									<?php echo $data['BahanBakar']; ?>
								</td>
								<td>
									<?php echo $data['WarnaTNKB']; ?>
								</td>
								<td>
									<?php echo $data['TahunRegistrasi']; ?>
								</td>
								<td>
									<?php echo $data['NoBPKB']; ?>
								</td>
								<td>
									<?php echo $data['KodeLokasi']; ?>
								</td>
								<td>
									<?php echo $data['MasaBerlakuSTNK']; ?>
								</td>
								<td>
									<img src="foto_motor/<?= $data['ID']; ?>.png" style="width: 200px; height: 150px;">
								</td>
								<td>
									<?php echo $data['TglBeli']; ?>
								</td>
								<td>
									<?php echo $data['HargaBeli']; ?>
								</td>
								<td>
									<?php echo $data['TglJual']; ?>
								</td>
								<td>
									<?php echo $data['HargaJual']; ?>
								</td>
							
								<td>
									<a href="?page=pemilik/edit_motor&ID=<?php echo $data['ID']; ?>" title="Ubah" class="btn btn-success">Ubah
										<i class="glyphicon glyphicon-edit"></i>
									</a>

									<a href="?page=pemilik/del_motor&ID=<?php echo $data['ID']; ?>" onclick="return confirm('Yakin Hapus Data Ini ?')" title="Hapus" class="btn btn-danger">Hapus
										<i class="glyphicon glyphicon-trash"></i>
									</a>
								</td>
							</tr>
						<?php
						}
						?>
					</tbody>

				</table>
			</div>
		</div>
	</div>
</section>