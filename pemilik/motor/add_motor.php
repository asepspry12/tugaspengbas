<?php
if ($_SESSION["ses_level"] !== "pemilik") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

include "koneksi.php";

$carikode = mysqli_query($koneksi, "SELECT MAX(ID) AS formatid FROM identitas_motor order by ID desc");
$datakode = mysqli_fetch_array($carikode);
$kode = $datakode['formatid'];
$urut = (int) substr($kode, 1, 3);
$urut++;

$char = '1';
$id_motor = $char . sprintf("%03s",$urut);

$tambah = mysqli_query($koneksi, "SELECT * FROM identitas_motor");
?>

<section class="content-header">
	<h1>
		Master Data
		<small>Data Motor</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Tambah Motor</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove">
							<i class="fa fa-remove"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="" method="post" enctype="multipart/form-data">
					<div class="box-body">

						<div class="form-group">
							<label>ID Motor</label>
							<input type="number" name="ID" id="ID" class="form-control" value="<?php echo $id_motor; ?>" readonly />
						</div>

						<div class="form-group">
							<label>No Registrasi</label>
							<input type="number" name="NoRegistrasi" id="NoRegistrasi" class="form-control" placeholder="No Registrasi">

						</div>

						<div class="form-group">
							<label>Nama Pemilik</label>
							<input type="text" name="NamaPemilik" id="NamaPemilik" class="form-control" placeholder="Nama Pemilik">
						</div>

						<div class="form-group">
							<label>Alamat</label>
							<input type="text" name="Alamat" id="Alamat" class="form-control" placeholder="Alamat">
						</div>

						<div class="form-group">
							<label>No Rangka</label>
							<input type="text" name="NoRangka" id="NoRangka" class="form-control" placeholder="No Rangka">

						</div>

						<div class="form-group">
							<label>No Mesin</label>
							<input type="number" name="NoMesin" id="NoMesin" class="form-control" placeholder="No Mesin">
						</div>

						<div class="form-group">

							<label>Plat Nomor</label>
							<input type="text" name="PlatNO" id="PlatNO" class="form-control" placeholder="Plat Nomor">

						</div>

						<div class="form-group">
							<label>Merk</label>
							<input type="text" name="Merk" id="Merk" class="form-control" placeholder="Merk motor">
						</div>

						<div class="form-group">
							<label>Tipe</label>
							<input type="text" name="Tipe" id="Tipe" class="form-control" placeholder="Tipe motor">
						</div>

						<div class="form-group">
							<label>Model</label>
							<input type="text" name="Model" id="Model" class="form-control" placeholder="Model motor">
						</div>

						<div class="form-group">
							<label>Tahun Pembuatan</label>
							<input type="number" name="TahunPembuatan" id="TahunPembuatan" class="form-control" placeholder="Tahun Pembuatan">
						</div>

						<div class="form-group">
							<label>Isi Silinder</label>
							<input type="text" name="IsiSilinder" id="IsiSilinder" class="form-control" placeholder="Isi Silinder">

						</div>

						<div class="form-group">
							<label>Bahan Bakar</label>
							<input type="text" name="BahanBakar" id="BahanBakar" class="form-control" placeholder="Bahan Bakar" >

						</div>

						<div class="form-group">
							<label>Warna TNKB</label>
							<input type="text" name="WarnaTNKB" id="WarnaTNKB" class="form-control" placeholder="Warna TNKB" >
						</div>

						<div class="form-group">
							<label>Tahun Registrasi</label>
							<input type="number" name="TahunRegistrasi" id="TahunRegistrasi" class="form-control" placeholder="Tahun Registrasi" >
						</div>

						<div class="form-group">
							<label>No BPKB</label>
							<input type="number" name="NoBPKB" id="NoBPKB" class="form-control" placeholder="No BPKB" >
						</div>

						<div class="form-group">
							<label>Kode Lokasi</label>
							<input type="text" name="KodeLokasi" id="KodeLokasi" class="form-control" placeholder="Kode Lokasi" >
						</div>

						<div class="form-group">
							<label>Masa Berlaku STNK</label>
							<input type="text" name="MasaBerlakuSTNK" id="MasaBerlakuSTNK" class="form-control" placeholder="Masa Berlaku STNK" >
						</div>

						<!-- Gambar -->
						<div class="form-group">
							<label>Gambar Motor</label>
							<input type="file" name="GambarMotor[]" id="GambarMotor" class="form-control" >
						</div>
						<!-- batas gambar -->

						<div class="form-group">
							<label>Tanggal Beli</label>
							<input type="date" name="TglBeli" id="TglBeli" class="form-control" >
						</div>

						<div class="form-group">
							<label>Harga Beli</label>
							<input type="text" name="HargaBeli" id="HargaBeli" class="form-control" placeholder="Rp. xxx.xxx" >
						</div>

						<div class="form-group">
							<label>Harga Jual</label>
							<input type="text" name="HargaJual" id="HargaJual" class="form-control" placeholder="Rp. xxx.xxx" >
						</div>

					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<input type="submit" name="Simpan" value="Simpan" class="btn btn-info">
						<a href="?page=pemilik/data_motor" class="btn btn-warning">Batal</a>
					</div>
				</form>
			</div>
			<!-- /.box -->
</section>

<?php

if (isset($_POST['Simpan'])) {

  // $i = $_GET[$format];
  // $id = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM identitas_motor WHERE ID = '$i' "));

  extract($_POST);
  $error = array();
  $extension = array("jpeg", "jpg", "png");
  $gid = $id_motor;
  foreach ($_FILES["GambarMotor"]["tmp_name"] as $key => $tmp_name) {
    
    $file_name2 = $_FILES["GambarMotor"]["name"][$key];
    $ext2 = pathinfo($file_name2,PATHINFO_EXTENSION);
    // $file_name = $id.'_'.$no.'.'.$ext2;
    $file_name = $gid.'.png';
    $file_tmp = $_FILES["GambarMotor"]["tmp_name"][$key];
    $ext = pathinfo($file_name,PATHINFO_EXTENSION);
    if(in_array($ext, $extension)) {
      move_uploaded_file($file_tmp = $_FILES["GambarMotor"]["tmp_name"][$key],"foto_motor/".$file_name);
    }

	$sql_simpan = "INSERT INTO identitas_motor (ID, NoRegistrasi, NamaPemilik, Alamat, NoRangka, NoMesin, PlatNO, Merk, Tipe, Model, TahunPembuatan, IsiSilinder, BahanBakar, WarnaTNKB, TahunRegistrasi, NoBPKB, KodeLokasi,MasaBerlakuSTNK,TglBeli,HargaBeli,HargaJual) VALUES (
	        '" . $id_motor . "', 
          '" . $_POST['NoRegistrasi'] . "',
          '" . $_POST['NamaPemilik'] . "',
          '" . $_POST['Alamat'] . "',
          '" . $_POST['NoRangka'] . "',
          '" . $_POST['NoMesin'] . "',
          '" . $_POST['PlatNO'] . "',
          '" . $_POST['Merk'] . "',
          '" . $_POST['Tipe'] . "',
          '" . $_POST['Model'] . "',
          '" . $_POST['TahunPembuatan'] . "',
          '" . $_POST['IsiSilinder'] . "',
          '" . $_POST['BahanBakar'] . "',
          '" . $_POST['WarnaTNKB'] . "',
          '" . $_POST['TahunRegistrasi'] . "',
          '" . $_POST['NoBPKB'] . "',
          '" . $_POST['KodeLokasi'] . "',
          '" . $_POST['MasaBerlakuSTNK'] . "',
          
          '" . $_POST['TglBeli'] . "',
          '" . $_POST['HargaBeli'] . "',
					'" . $_POST['HargaJual'] . "' ) ";
	$query_simpan = mysqli_query($koneksi, $sql_simpan);
	// mysqli_close($koneksi);

	if ($query_simpan) {

		echo "<script>
      Swal.fire({title: 'Tambah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
      }).then((result) => {
          if (result.value) {
              window.location = 'index.php?page=pemilik/data_motor';
          }
      })</script>";
	} else {
		echo "<script>
      Swal.fire({title: 'Tambah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
      }).then((result) => {
          if (result.value) {
              window.location = 'index.php?page=pemilik/add_motor';
          }
      })</script>";
	}
 }
}

?>