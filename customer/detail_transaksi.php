<?php
if ($_SESSION["ses_level"] !== "customer") {
    echo "<script>
		window.location = 'login.php';
	</script>";
}


$carikode = mysqli_query($koneksi, "SELECT MAX(IdTrsk) AS formatid FROM transaksi order by IdTrsk desc");
$datakode = mysqli_fetch_array($carikode);
$kode = $datakode['formatid'];
$urut = (int) substr($kode, 1, 3);
$urut++;

$char = '5';
$id_transaksi = $char . sprintf("%03s",$urut);

$tambah = mysqli_query($koneksi, "SELECT * FROM transaksi");


$guser = $_SESSION['ses_id'];
$user = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM user WHERE IDUser='".$guser."'"));
$id_user = $user['IDUser']; 

$id_motor = $_GET['ID'];
$motor = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM identitas_motor WHERE ID = '".$id_motor."' "));


?>

<section class="content-header">
    <h1>
        Menu Beli Motor
        <small>Detail Motor</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.php">
                <i class="fa fa-home"></i>
                <b>Si Barokah Motor</b>
            </a>
        </li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Beli Motor</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove">
                            <i class="fa fa-remove"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="box-body">

                        <center>
                        <img src="foto_motor/<?php echo $motor['ID']; ?>.png" alt="" class="img-fluid card-img-top" style=" width: 400px; height: 300px;">
                        </center>

                        <br>

                        <div class="form-group">
                            <label>ID Transaksi</label>
                            <input type="number" name="IdTrsk" id="IdTrsk" class="form-control" value="<?php echo $id_transaksi; ?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Transaksi</label>
                            <input type="date" name="TglTrans" id="TglTrans" class="form-control" >
                        </div>

                        <div class="form-group">
                            <label>ID Customer</label>
                            <input type="number" name="IDUser" id="IDUser" class="form-control" value="<?php echo $id_user; ?>" readonly />
                        </div>    

                        <div class="form-group">
                            <label>ID Kendaraan</label>
                            <input type="number" name="ID" id="ID" class="form-control" value="<?php echo $motor['ID']; ?>" readonly/>
                        </div>

                        <div class="form-group">
                            <label>Harga Jual</label>
                            <input type="text" name="HargaJual" id="HargaJual" class="form-control" value="Rp. <?php echo $motor['HargaJual'] ?>" >
                        </div>

                        <div class="form-group">
                            <label>Harga Fix</label>
                            <input type="text" name="HargaJualReal" id="HargaJualReal" class="form-control" placeholder="Rp. xxx.xxx" >
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <input type="submit" name="Beli" value="Beli" class="btn btn-info">
                        <a href="?page=customer/transaksi" class="btn btn-warning">Batal</a>
                    </div>
                </form>
            </div>
            <!-- /.box -->
</section>

<?php


if (isset($_POST['Beli'])) {
    //mulai proses simpan data
    $sql_simpan = "INSERT INTO transaksi (Idtrsk,TglTrans,IdCust,IdKenda,HargaJual,HargaJualReal) VALUES (
        '" . $id_transaksi . "',
        '" . $_POST['TglTrans'] . "',
        '" . $id_user . "',
        '" . $id_motor . "',
        '" . $motor['HargaJual'] . "',
        '" . $_POST['HargaJualReal'] . "'
        )";
    $query_simpan = mysqli_query($koneksi, $sql_simpan);

    $query_update = mysqli_query($koneksi, "UPDATE identitas_motor SET TglJual = '".$_POST['TglTrans']."' WHERE ID = '".$id_motor."'");
    if ($query_simpan && $query_update) {
        echo "<script>
      Swal.fire({title: 'Tambah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
      }).then((result) => {if (result.value){
        window.location = 'index.php?page=customer/transaksi';
        }
      })</script>";
    } else {
        echo "<script>
      Swal.fire({title: 'Tambah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
      }).then((result) => {if (result.value){
        window.location = 'index.php?page=customer/detail_transaksi&IdTrans=<?= $id_transaksi; ?>';
        }
      })</script>";
    }
    //selesai proses simpan data
}
