<?php
if ($_SESSION["ses_level"] !== "teller") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

// include 'inc/koneksi.php';
if (isset($_GET['IdTrsk'])) {
	$ID = $_GET['IdTrsk'];
	$sql_cek = "SELECT * FROM transaksi WHERE IdTrsk = '".$ID."' ";
	$query_cek = mysqli_query($koneksi, $sql_cek);
	$data_cek = mysqli_fetch_array($query_cek, MYSQLI_BOTH);
}

$gid = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM transaksi ORDER BY IdTrsk DESC "));
?>

<section class="content-header">
	<h1>
		Master Data
		<small>Data Motor</small>
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Ubah Motor</h3>
					<div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
						<button type="button" class="btn btn-box-tool" data-widget="remove">
							<i class="fa fa-remove"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form action="" method="post" enctype="multipart/form-data">
					<div class="box-body">

						<div class="form-group">
							<label>Id Transaksi</label>
							<input type='text' class="form-control" name="IdTrsk" value="<?php echo $data_cek['IdTrsk']; ?>" readonly />
						</div>

						<div class="form-group">
							<label>Tanggal Transaksi</label>
							<input type='text' class="form-control" name="TglTrans" value="<?php echo $data_cek['TglTrans']; ?>" readonly />

						</div>


						<div class="form-group">
							<label>Id Customer</label>
							<input type='text' class="form-control" name="IdCust" value="<?php echo $data_cek['IdCust']; ?>" readonly/>

						</div>

						<div class="form-group">
							<label>Id Kendaraan</label>
							<input class="form-control" name="ID" value="<?php echo $data_cek['IdKenda']; ?>"  readonly />
						</div>

						<div class="form-group">
							<label>Harga Jual</label>
							<input class="form-control" name="HargaJual" value="<?php echo $data_cek['HargaJual']; ?>" readonly />
						</div>

						<div class="form-group">
							<label>Harga Jual Real</label>
							<input class="form-control" name="HargaJualReal" value="<?php echo $data_cek['HargaJualReal']; ?>" />
						</div>



					</div>
					<!-- /.box-body -->

					<div class="box-footer">
						<input type="submit" name="Ubah" value="Ubah" class="btn btn-success">
						<a href="?page=teller/transaksi" class="btn btn-warning">Batal</a>
					</div>
				</form>
			</div>
			<!-- /.box -->
</section>

<?php


if (isset($_POST['Ubah'])) {


 

	$sql_ubah = "UPDATE transaksi SET
        HargaJualReal ='" . $_POST['HargaJualReal'] ."' 
        WHERE IdTrsk='" . $_POST['IdTrsk'] . "' ";
	
	$query_ubah = mysqli_query($koneksi, $sql_ubah);

	if ($query_ubah) {
		echo "<script>
	    Swal.fire({title: 'Ubah Data Berhasil',text: '',icon: 'success',confirmButtonText: 'OK'
	    }).then((result) => {
	        if (result.value) {
	            window.location = 'index.php?page=teller/transaksi';
	        }
	    })</script>";
	} 
	else {
		echo "<script>
	    Swal.fire({title: 'Ubah Data Gagal',text: '',icon: 'error',confirmButtonText: 'OK'
	    }).then((result) => {
	        if (result.value) {
	            window.location = 'index.php?page=teller/transaksi';
	        }
	    })</script>";
	}
   
  }
