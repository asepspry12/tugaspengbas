-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2022 at 04:14 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjualan_motor_bekas`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `IdCust` int(255) NOT NULL,
  `NamaCust` varchar(50) NOT NULL,
  `AlamatCust` varchar(200) NOT NULL,
  `TelpCust` int(15) NOT NULL,
  `NIKCust` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`IdCust`, `NamaCust`, `AlamatCust`, `TelpCust`, `NIKCust`) VALUES
(9006, 'Puput', 'Bogor', 89923781, 32789020713),
(9007, 'Wildanz', 'Bekasi', 86283721, 327235467436),
(9008, 'Sarahh', 'Cikampek', 8523632, 3249428825),
(9009, 'Asepp', 'Karawang', 8893278, 32498923214);

-- --------------------------------------------------------

--
-- Table structure for table `identitas_motor`
--

CREATE TABLE `identitas_motor` (
  `ID` int(15) NOT NULL,
  `NoRegistrasi` int(15) NOT NULL,
  `NamaPemilik` varchar(255) NOT NULL,
  `Alamat` varchar(255) NOT NULL,
  `NoRangka` varchar(15) NOT NULL,
  `NoMesin` int(15) NOT NULL,
  `PlatNO` varchar(8) NOT NULL,
  `Merk` varchar(30) NOT NULL,
  `Tipe` varchar(30) NOT NULL,
  `Model` varchar(30) NOT NULL,
  `TahunPembuatan` int(4) NOT NULL,
  `IsiSilinder` varchar(8) NOT NULL,
  `BahanBakar` varchar(30) NOT NULL,
  `WarnaTNKB` varchar(30) NOT NULL,
  `TahunRegistrasi` int(4) NOT NULL,
  `NoBPKB` int(30) NOT NULL,
  `KodeLokasi` varchar(4) NOT NULL,
  `MasaBerlakuSTNK` varchar(30) NOT NULL,
  `GambarMotor` varchar(100) NOT NULL,
  `TglBeli` date NOT NULL,
  `HargaBeli` int(30) NOT NULL,
  `TglJual` date NOT NULL,
  `HargaJual` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `identitas_motor`
--

INSERT INTO `identitas_motor` (`ID`, `NoRegistrasi`, `NamaPemilik`, `Alamat`, `NoRangka`, `NoMesin`, `PlatNO`, `Merk`, `Tipe`, `Model`, `TahunPembuatan`, `IsiSilinder`, `BahanBakar`, `WarnaTNKB`, `TahunRegistrasi`, `NoBPKB`, `KodeLokasi`, `MasaBerlakuSTNK`, `GambarMotor`, `TglBeli`, `HargaBeli`, `TglJual`, `HargaJual`) VALUES
(1003, 1842123, 'jayadiwangsa', 'karawang', '123', 1233, '4452', 'supra x', '125', 'honda', 2012, '120', 'bensin', 'merah', 2012, 1234567, 'T', '5', '', '2022-02-01', 22000000, '2022-02-10', 20000000),
(1004, 21388, 'Junaidi', 'Jakarta', '71263', 92183, 'T9744JUY', 'Suzuki', 'Aerox', 'Matic', 2020, '125cc', 'Bensin', 'Putih', 2020, 291893, 'T', '5', '', '2021-05-05', 28000000, '2021-12-17', 25000000),
(1005, 87284, 'Jhony', 'Karawang', '5arsay4vb', 812397, 'K9823PRE', 'Kawasaki', 'Ninja', 'Kopling', 2020, '125cc', 'Bensin', 'Putih', 2020, 912738, 'B', '5 Tahun', '', '2021-07-17', 30000000, '0000-00-00', 28000000),
(1006, 38436, 'Kosim', 'Bandung', 'S89Y76E', 781536, 'D5670VBN', 'Yamaha', 'Freego', 'Kopling', 2020, '220cc', 'Bensin', 'Putih', 2020, 873265, 'BBaw', '5 Tahun', 'motor kawasaki ninja.jpg', '2021-12-01', 40000000, '0000-00-00', 37000000),
(1007, 893278, 'Cukani', 'Padang', 'S8DY7G', 73265, 'E7683SVB', 'Honda', 'CBR', 'Kopling', 2021, '220cc', 'Bensin', 'Hitam', 2021, 327693, 'E', '5 Tahun', '', '0000-00-00', 35000000, '2022-03-10', 33000000),
(1008, 376428, 'Verdi', 'Medan', 'SGRW7', 428972, 'V7326TWE', 'Vespa', 'Vespa', 'Matic', 2020, '220cc', 'Bensin', 'Putih', 2020, 892347, 'V', '5 Tahun', '', '2021-09-16', 32000000, '0000-00-00', 30000000),
(1009, 98537, 'Puji', 'Surabaya', 'FE6G7W', 892463, 'S5748IEW', 'Suzuki', 'Ninja', 'Kopling', 2018, '200cc', 'Bensin', 'Putih', 2018, 923476, 'S', '5 Tahun', '', '2019-06-20', 34000000, '2022-01-20', 32000000),
(1010, 983223, 'Opang', 'Subang', 'DUW287D', 3985993, 'K4837EWQ', 'Vespa', 'Vespa', 'Matic', 2017, '125cc', 'Bensin', 'Putih', 2017, 910378824, 'K', '5 Tahun', '', '2021-06-18', 25000000, '0000-00-00', 24000000);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `IdTrsk` int(20) NOT NULL,
  `TglTrans` date NOT NULL,
  `IdCust` int(255) NOT NULL,
  `IdKenda` int(15) NOT NULL,
  `HargaJual` int(30) NOT NULL,
  `HargaJualReal` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`IdTrsk`, `TglTrans`, `IdCust`, `IdKenda`, `HargaJual`, `HargaJualReal`) VALUES
(5001, '2022-02-10', 9005, 1003, 20000000, 17000000),
(5002, '2021-12-17', 9005, 1004, 25000000, 24000000),
(5003, '2021-10-15', 9005, 1007, 33000000, 31000000),
(5004, '0000-00-00', 9005, 1009, 32000000, 28000000),
(5005, '2021-08-19', 9005, 1009, 32000000, 30000000),
(5006, '2022-01-20', 9006, 1009, 32000000, 30000000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `IDUser` int(255) NOT NULL,
  `Nama` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Hak_Akses` varchar(255) NOT NULL,
  `Create_Date` datetime NOT NULL DEFAULT current_timestamp(),
  `Jabatan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`IDUser`, `Nama`, `Password`, `Hak_Akses`, `Create_Date`, `Jabatan`) VALUES
(9001, 'Junaedi', 'Junaedi', 'teknisi', '2021-12-20 20:41:29', 'Teknisi'),
(9002, 'Wildan', 'Wildan', 'pemilik', '2021-12-20 20:42:11', 'CEO'),
(9003, 'Tawang', 'Tawang', 'teller', '2021-12-20 20:44:35', 'Teller'),
(9004, 'Asep', 'Asep', 'teknisi', '2021-12-20 20:45:05', 'Teknisi'),
(9006, 'Puput', 'Puput', 'customer', '2022-01-05 19:25:51', 'Tukang Donat'),
(9007, 'Wildanz', 'Wildanz', 'pemilik', '2022-01-05 20:30:47', 'CEO'),
(9008, 'Sarahh', 'Sarahh', 'teller', '2022-01-05 20:45:09', 'Teller'),
(9009, 'Asepp', 'Asepp', 'teknisi', '2022-01-05 20:55:21', 'Teknisi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`IdCust`);

--
-- Indexes for table `identitas_motor`
--
ALTER TABLE `identitas_motor`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`IdTrsk`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`IDUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `IdCust` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9010;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
