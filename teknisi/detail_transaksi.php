<?php
if ($_SESSION["ses_level"] !== "teknisi") {
    echo "<script>
		window.location = 'login.php';
	</script>";
}
if ($_GET['ID']) {
    $ID = $_GET['ID'];
}
?>

<section class="content-header">
    <h1>
        Menu Beli Motor
        <small>Detail Motor</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="index.php">
                <i class="fa fa-home"></i>
                <b>Si Barokah Motor</b>
            </a>
        </li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove">
                    <i class="fa fa-remove"></i>
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pemilik Di STNK</th>
                            <th>Alamat</th>
                            <th>Plat No</th>
                            <th>Merk</th>
                            <th>Type</th>
                            <th>Model</th>
                            <th>Tahun Pembuatan</th>
                            <th>Isi Silinder</th>
                            <th>Tahun Registrasi</th>
                            <th>Gambar</th>
                            <th>Tanggal Jual</th>
                            <th>Harga Jual</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $no = 1;
                        $sql = $koneksi->query("SELECT * from identitas_motor WHERE ID = '" . $ID . "'");
                        while ($data = $sql->fetch_assoc()) {
                        ?>

                            <tr>
                                <td>
                                    <?php echo $no++; ?>
                                </td>
                                <td>
                                    <?php echo $data['NamaPemilik']; ?>
                                </td>
                                <td>
                                    <?php echo $data['Alamat']; ?>
                                </td>
                                <td>
                                    <?php echo $data['PlatNO']; ?>
                                </td>
                                <td>
                                    <?php echo $data['Merk']; ?>
                                </td>
                                <td>
                                    <?php echo $data['Tipe']; ?>
                                </td>
                                <td>
                                    <?php echo $data['Model']; ?>
                                </td>
                                <td>
                                    <?php echo $data['TahunPembuatan']; ?>
                                </td>
                                <td>
                                    <?php echo $data['IsiSilinder']; ?>
                                </td>
                                <td>
                                    <?php echo $data['TahunRegistrasi']; ?>
                                </td>
                                <td>
                                    <img src="foto_motor/<?= $data['ID']; ?>.png">
                                </td>
                                <td>
                                    <?php echo $data['TglJual']; ?>
                                </td>
                                <td>
                                    <?php echo $data['HargaJual']; ?>
                                </td>

                                <td>
                                    <!-- <a href="?page=pemilik/edit_motor&ID=<?php echo $data['ID']; ?>" title="Ubah" class="btn btn-success"> -->Bayar
                                    <i class="glyphicon glyphicon-edit"></i>
                                    </a>

                                    <!-- <a href="?page=pemilik/del_motor&ID=<?php echo $data['ID']; ?>" onclick="return confirm('Yakin Hapus Data Ini ?')" title="Hapus" class="btn btn-danger"> -->Hapus
                                    <i class="glyphicon glyphicon-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
</section>