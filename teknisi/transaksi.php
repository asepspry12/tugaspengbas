<?php

if ($_SESSION["ses_level"] !== "teknisi") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

?>
<section class="content-header">
	<h1>
		Menu Beli Motor
	</h1>
	<ol class="breadcrumb">
		<li>
			<a href="index.php">
				<i class="fa fa-home"></i>
				<b>Barokah Motor</b>
			</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header with-border">
			<!-- <div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div> -->
		</div>
		<!-- box-header -->

		<div class="row">
			<?php
			$gid = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM identitas_motor ORDER BY ID DESC "));
			$sql = mysqli_query($koneksi, "SELECT * FROM identitas_motor ");
			while ($data = mysqli_fetch_assoc($sql)) {
			?>

				<!-- Gallery item -->
				<div class="col-xl-3 col-lg-4 col-md-6 mb-4" style=" padding: 20px;">
					<div class="bg-white rounded shadow-sm"><a href="?page=teknisi/detail_transaksi&ID=<?= $data['ID']; ?>"><img src="foto_motor/<?php echo $data['ID']; ?>.png" alt="" class="img-fluid card-img-top" style="width: 200px; height: 150px;"></a>
						<div class="p-4">
							<h5><b><a href="?page=teknisi/detail_transaksi&ID=<?= $data['ID']; ?>"><?php echo $data['Merk']; ?></a></b></h5>
							<p class="small text-muted mb-0"><?php echo $data['TahunRegistrasi']; ?></p>
							<h5 style="color:red;"><b>Rp. <?php echo $data['HargaJual']; ?></b></h5>
							<div class="d-flex align-items-center justify-content-between rounded-pill bg-light px-3 py-2 mt-4">

								<!-- <p class="small mb-0"><i class="fa fa-picture-o mr-2"></i><span class="font-weight-bold">Beli Motor</span></p>
 -->
								<div class="badge badge-danger px-3 rounded-pill font-weight-normal">
									<a href="?page=teknisi/del_transaksi&ID=<?php echo $data['ID']; ?>" onclick="return confirm('Yakin Hapus Data Ini ?')" title="Hapus" style="color: white">Delete</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Gallery item -->

			<?php
			}
			?>

			<!-- </div>
    <div class="py-5 text-right"><a href="#" class="btn btn-dark px-5 py-3 text-uppercase">Show me more</a></div> -->


			<!-- batas main content -->
		</div>
	</div>
</section>
