<?php
include 'koneksi.php';

if ($_SESSION["ses_level"] !== "teknisi") {
	echo "<script>
		window.location = 'login.php';
	</script>";
}

$user = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM user WHERE IDUser='".$_SESSION['ses_id']."'"));


?>

<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove">
					<i class="fa fa-remove"></i>
				</button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="table-responsive">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Id Transaksi</th>
							<th>Tanggal Transaksi</th>
							<th>Nama Customer</th>
							<th>Merk Kendaraan</th>
							<th>Harga Jual</th>
							<th>Harga Jual Real</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>


				<?php
						$no = 1;
						$sql = mysqli_query($koneksi, "SELECT * FROM transaksi INNER JOIN identitas_motor ON transaksi.IdKenda = identitas_motor.ID INNER JOIN customer ON transaksi.IdCust = customer.IdCust ORDER BY transaksi.IdTrsk DESC ");
						while ($data = $sql->fetch_assoc()) {
						?>

							<tr>
								<td>
									<?php echo $no++; ?>
								</td>
								<td>
									<?php echo $data['IdTrsk']; ?>
								</td>
								<td>
									<?php echo $data['TglTrans']; ?>
								</td>
								<!-- <td>
								<?php echo $data['IdCust']; ?>
							</td> -->
								<td>
									<?php echo $data['NamaCust']; ?>
								</td>
								<!-- <td>
								<?php echo $data['IdKenda']; ?>
							</td> -->
								<td>
									<?php echo $data['Merk']; ?>
								</td>
								<td>
									<?php echo $data['HargaJual']; ?>
								</td>
								<td>
									<?php echo $data['HargaJualReal']; ?>
								</td>
							<td>
								<a href="?page=teknisi/del_transaksi1&IdTrsk=<?php echo $data['IdTrsk']; ?>" onclick="return confirm('Yakin Hapus Data Ini ?')" title="Hapus" class="btn btn-danger">Hapus
								<i class="glyphicon glyphicon-trash"></i>
								</a>
							</td>
						</tr>

						<?php
						}
						?>
					</tbody>

				</table>
			</div>
		</div>
	</div>
</section>